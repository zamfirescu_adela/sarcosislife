
if ('serviceWorker' in navigator) {
    navigator.serviceWorker.register('/sw.js', {scope: '/sw-test/'})
        .then(function(reg) {
            // registration worked
            console.log('Registration succeeded. Scope is ' + reg.scope);
        }).catch(function(error) {
        // registration failed
        console.log('Registration failed with ' + error);
    });
}


// self.addEventListener('install', (event) => {
//     console.info('Event: Install');
//
//     event.waitUntil(
//         caches.open(cacheName)
//             .then((cache) => {
//                 //[] of files to cache & if any of the file not present `addAll` will fail
//                 return cache.addAll(files)
//                     .then(() => {
//                         console.info('All files are cached');
//                         return self.skipWaiting(); //To forces the waiting service worker to become the active service worker
//                     })
//                     .catch((error) =>  {
//                         console.error('Failed to cache', error);
//                     })
//             })
//     );
// });
//
//
// self.addEventListener('fetch', (event) => {
//     console.info('Event: Fetch');
//
//     var request = event.request;
//
//     //Tell the browser to wait for newtwork request and respond with below
//     event.respondWith(
//         //If request is already in cache, return it
//         caches.match(request).then((response) => {
//             if (response) {
//                 return response;
//             }
//
//             //if request is not cached, add it to cache
//             return fetch(request).then((response) => {
//                 var responseToCache = response.clone();
//                 caches.open(cacheName).then((cache) => {
//                     cache.put(request, responseToCache).catch((err) => {
//                         console.warn(request.url + ': ' + err.message);
//                     });
//                 });
//
//                 return response;
//             });
//         })
//     );
// });
//
// /*
//   ACTIVATE EVENT: triggered once after registering, also used to clean up caches.
// */
//
// //Adding `activate` event listener
// self.addEventListener('activate', (event) => {
//     console.info('Event: Activate');
//
//     //Remove old and unwanted caches
//     event.waitUntil(
//         caches.keys().then((cacheNames) => {
//             return Promise.all(
//                 cacheNames.map((cache) => {
//                     if (cache !== cacheName) {     //cacheName = 'cache-v1'
//                         return caches.delete(cache); //Deleting the cache
//                     }
//                 })
//             );
//         })
//     );
// });
//
//
//
// const publicVapidKey =
//     "BJthRQ5myDgc7OSXzPCMftGw-n16F7zQBEN7EUD6XxcfTTvrLGWSIG7y_JxiWtVlCFua0S8MTB5rPziBqNx1qIo";
//
// // Check for service worker
// if ("serviceWorker" in navigator) {
//     send().catch(err => console.error(err));
// }
//
// var CACHE_NAME = 'my-site-cache-v1';
// var urlsToCache = [
//     '/',
//     '/css/main.css',
//     '/js/main.js'
// ];
//
// self.addEventListener('install', function(event) {
//     // Perform install steps
//     event.waitUntil(
//         caches.open(CACHE_NAME)
//             .then(function(cache) {
//                 console.log('Opened cache');
//                 return cache.addAll(urlsToCache);
//             })
//     );
// });
//
//
// // Register SW, Register Push, Send Push
// async function send() {
//     // Register Service Worker
//     console.log("Registering service worker...");
//     const register = await navigator.serviceWorker.register("/worker.js", {
//         scope: "/"
//     });
//     console.log("Service Worker Registered...");
//
//     // Register Push
//     console.log("Registering Push...");
//     const subscription = await register.pushManager.subscribe({
//         userVisibleOnly: true,
//         applicationServerKey: urlBase64ToUint8Array(publicVapidKey)
//     });
//     console.log("Push Registered...");
//
//     // Send Push Notification
//     console.log("Sending Push...");
//     await fetch("/subscribe", {
//         method: "POST",
//         body: JSON.stringify(subscription),
//         headers: {
//             "content-type": "application/json"
//         }
//     });
//     console.log("Push Sent...");
// }
//
// function urlBase64ToUint8Array(base64String) {
//     const padding = "=".repeat((4 - base64String.length % 4) % 4);
//     const base64 = (base64String + padding)
//         .replace(/\-/g, "+")
//         .replace(/_/g, "/");
//
//     const rawData = window.atob(base64);
//     const outputArray = new Uint8Array(rawData.length);
//
//     for (let i = 0; i < rawData.length; ++i) {
//         outputArray[i] = rawData.charCodeAt(i);
//     }
//     return outputArray;
// }
