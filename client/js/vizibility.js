const app = {
    ButtonLoginToRegister: document.querySelector("#btnLogInRegister"),
    ButtonLoginToMainPage: document.querySelector("#btnLogIn"),
    ButtonRegister: document.querySelector("#register-btn"),

    LoginPage: document.querySelector("#login-page"),
    RegisterPage: document.querySelector("#register-page"),
    MainPage: document.querySelector("#main-page"),


    show: (element)=> element.setAttribute('style', 'display:inherit'),
    hide: (element)=> element.setAttribute('style', 'display:none'),
};


app.ButtonLoginToRegister.addEventListener('click', () => {
    app.hide(app.LoginPage);
    app.show(app.RegisterPage);
})

app.ButtonLoginToMainPage.addEventListener('click', () => {
    let value={
        username:document.getElementById('userName').value,
        password: document.getElementById('userPassword').value
    }

    axios.post('/auth/login', value)
        .then(function (response) {
            console.log(response);
            app.hide(app.LoginPage);
            app.show(app.MainPage);
        })
        .catch(function (error) {
            console.log(error);
        });

    app.hide(app.LoginPage);
    app.show(app.MainPage);
})

app.ButtonRegister.addEventListener('click', () => {
    let value = {
        name: document.getElementById('numeUser').value,
        email: document.getElementById('numeEmail').value,
        series: document.getElementById('input-serie').value,
        group: document.getElementById('input-grupa').value,
        password: document.getElementById('numeParola').value
    }

    axios.post('/auth/register', value)
        .then((response) => {
            app.hide(app.RegisterPage);
            app.show(app.LoginPage);
            console.log(response);
        })
        .catch(function (error) {
            console.log(error);
        });
})