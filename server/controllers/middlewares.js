
const User = require('../models').User;

//we should insert a token for every user
module.exports.LogInCheck = (req, res, next) => {

    User.findOne({
        where: {
            token: req.session.token,
            id: req.session.id
        },
        raw: true
    }).then((u) => {
        if (!u) {
            res.status(403).send({message: "Forbidden"});
        }
        else {
            next();
        }
    })
}


//we should apply this middleware on 
module.exports.InterviewAlreadyBooked = (req, res, next) => {

    UserInterview.findOne({
        attributes: ['date', 'time'],
        where: {
            //use token
            user_id: req.session.id
        },
        raw: true
    }).then((result) => {

        if (result === null) {
            next();
        }
        else {
            res.status(302).send(result);
        }

    });

}

module.exports.TestAlreadyBooked = (req, res, next) => {

    Result.findOne({
        where: {
            user_id: req.session.id,
            position_id: req.params.position_id,
            isFinished: 1
        },
        raw: true
    }).then((result) => {
        if (result === null) {
            next();
        }
        else {
            res.status(402).send({message: "Your answers were registered!"});
        }
    });
}

module.exports.isAdmin = (req, res, next) => {

    User.findOne({
        where: {
            token: req.session.token,
            id: req.session.id
        },
        raw: true
    }).then((u) => {

        if (u && u.isAdmin === 2) {
            next();
        }
        else {
            res.status(403).send("Forbidden");
        }
    });
};

module.exports.isViewer = (req, res, next) => {

    User.findOne({
        where: {
            token: req.session.token,
            id: req.session.id
        },
        raw: true
    }).then((u) => {

        if (u && (u.isAdmin === 1 || u.isAdmin === 2)) {
            next();
        }
        else {
            res.status(403).send("Forbidden");
        }
    });
};

module.exports.ForceAdmin = (req, res, next) => {

    if (req.body.force === "admin_password") {
        next();
    }
    else {
        res.status(403).send("Forbidden");
    }
}


module.exports.TimeExpiration = (req, res, next) => {

    Position.findOne({
        where: {
            id: req.params.position_id
        }
    }).then((position) => {


        Result.findOne({
            where:
                {
                    user_id: req.session.id,
                    position_id: req.params.position_id
                },
            raw: true
        }).then((result) => {

            if (result) {
                let now = Date.now();

                if ((now - result.started) > position.duration) {
                    Result.update({
                            isFinished: 1
                        },
                        {
                            where: {
                                user_id: req.session.id,
                                position_id: req.params.position_id
                            }
                        }).then(() => {
                        res.status(403).send({message: "Time expired"});
                    })
                } else {
                    next();
                }
            } else {

                Result.create({
                    answers: null,
                    started: Date.now(),
                    finished: null,
                    isFinished: 0,
                    user_id: req.session.id,
                    position_id: req.params.position_id
                });

                next();
            }
        })

    }).catch(() => res.status(404).send("Not found"));

}