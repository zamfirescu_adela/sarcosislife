'use strict';
const emailConfig = require('../conf/email');
const mailgun = require('mailgun-js')(emailConfig);
const model = require('../models');
const path = require('path');

module.exports.recreateTables = (req, res) => {
    model.connect.sync({force: true})
        .then(async () => {
            await model.User.create({
                name: 'Admin',
                email: 'admin',
                password: 'admin',
                token: Math.random().toString(36),
                createdAt: Date.now()
            })

            await model.Subject.create({
                name: 'POO',
                noCredit: 4,
                year: 2,
                semester: 1,
                mandatory: true,
            })
            await model.Subject.create({
                name: 'ATP',
                noCredit: 5,
                year: 1,
                semester: 2,
                mandatory: true,
            })

            await model.Professor.create({
                name: 'Uscatu Cristian',
                email: 'cristian.uscatu@ase.ro'
            })
            await model.ClassRoom.create({
                code: '2018',
                building: 'Virgil Madgearu',
                type: 'S'
            })
            await model.Series.create({
                name: "Seria X",
            })
            await model.Group.create({
                name: "9001",
                series_id: 1
            })
            await model.SubjectGroup.create({
                type: "P",
                class_room_id: 1,
                professor_id: 1,
                subject_id: 1,
                group_id: 1
            })
            await model.SubjectSeries.create({
                class_room_id: 1,
                professor_id: 1,
                subject_id: 1,
                group_id: 1
            })

            await model.User.create({
                name: 'Mihai',
                email: 'mihai@aa.com',
                password: 'mihai',
                group: 1,
                token: Math.random().toString(36),
                createdAt: Date.now()
            })

            await model.CustomGroup.create({
                name: "No Name Group",
                subject_id: 1,
                leader: 1
            })

            await model.SubjectCustomGroupActivity.create({
                name: "CustomGroupActivity",
                start: "2018-03-25T12:00:00Z",
                deadline: "2018-03-30T12:00:00Z",
                leader: 1
            })


            res.status(201).send('recreated all tables')
        })
        .catch(() => res.status(500).send('hm, that was unexpected...'))
};


module.exports.initialLogin = (req, res) => {


    model.User.findOne({
        attributes: ['id', 'token', 'name'],
        where: {
            email: req.body.email,
            password: req.body.password
        },
        raw: true
    }).then((u) => {

        if (!u) {
            res.status(403).send("Incorrect email/password");
        }
        else {

            if (req.session.id) {

                res.status(202).send({
                    name: u.name,
                    text: `Already logged in, ${u.name}`,
                });
            }
            else {
                model.User.update({
                    active: 1, lastLogin: Date.now()
                }, {returning: true, where: {id: u.id}});

                req.session.id = u.id;
                req.session.token = u.token;


                res.status(200).send({name: u.name, text: `Welcome, ${u.name}`});


            }
        }
    });

};


module.exports.register = async (req, res) => {
    const series = await model.Series.findOne({where: {name: req.body.series}, raw: true});
    const group = await model.Group.findOne({where: {name: req.body.group}, raw: true});
    const email = req.body.email.toLowerCase().replace(/(\+[a-z0-9]*)/i, '').replace(/(\.{2,})/g, '.');

    if (!email.match('(?:[a-z0-9!#$%&\'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&\'*+/=?^_`{|}~-]+)*|"(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-\x7f]|\\\\[\x01-\x09\x0b\x0c\x0e-\x7f])*")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21-\x5a\x53-\x7f]|\\\\[\x01-\x09\x0b\x0c\x0e-\x7f])+)\\])'))
        res.status(400).send('invalid email');
    else if (!req.body.name.match('([A-ZĂÎȘȚÂ])+(?=.{1,40}$)[a-zA-ZĂÎȘȚÂăîșț]+(?:[-\\s][a-zA-ZĂÎȘȚÂăîșțâ]+)*$'))
        res.status(400).send('invalid name');
    else if (req.body.password.length < 3)
        res.status(400).send('invalid password');
    else
        model.User.findOne({
            attributes: ['email'],
            where: {
                email: email
            },
            raw: true
        }).then((u) => {

            if (u) {
                res.status(400).send(`Email ${u.email} have already an account.`);
            }
            else {
                model.User.create({
                    name: req.body.name,
                    email: email,
                    group_id: group.id,
                    series_id: series.id,
                    password: req.body.password,
                    token: Math.random().toString(36),
                    privileges: 0,
                    createdAt: Date.now()
                })
                    .then(user => {
                        const data = {
                            from: `"Sarcos Team" <info@ats-app.me>`,
                            to: user.email,
                            subject: 'Sarcos Account Creation',
                            text: `Hello ${user.name},\nCongratulations for choosing to register to Sarcos. For finishing the registration you have 3 hours to login on the web page, otherwise you will have to repeat the process.\n\n Here you will have the credentials for accessing the application.\n\nUsername: ${user.email}\nPassword: ${user.password}\n\nHave a nice day!\nSarcos Team`,
                            html: `<p>Hello ${user.name},</p><p>Congratulations for choosing to register to Sarcos. For finishing the registration you have 3 hours to login on the web page, otherwise you will have to repeat the process.</p><p> Here you will have the credentials for accessing the application.</p><ul style=" list-style-type: none;"><li>Username:  ${user.email}</li><li>Password: ${user.password}</li></ul><p>Have a nice day!<br>Sarcos Team</p>`

                        };

                        mailgun.messages().send(data, function (error, body) {
                            console.log(body);
                            if (error) {
                                console.log(`Student ${user.email} didn't receive a email.`);
                                res.status(500).send("Email couldn't be sent. Please contact the team.")

                            } else res.status(201).send("Success");

                        });


                    })
                    .catch(() => {
                        res.status(500).send("Server error")
                    });

            }

        })
}

module.exports.Logout = (req, res) => {

    req.session.reset();
    res.status(200).send("You've been logged out");
}


