const Group = require('../models').Group;
const Series = require('../models').Series;
const model = require('../models');

module.exports.getGroups = async (req, res) => {
    let array = [];
    let series = await Series.findAll();

    array = await Promise.all(series.map(async (sery) => {
        let value = {series: sery.name, groups: []};
        let x = await Group.findAll({where: {series_id: sery.id}, raw: true});
        Promise.all(x.map(y => {
            value.groups.push(y.name)
        }))

        return value;
    }))


    res.status(200).send(array);

};

module.exports.addSeriesAndGroups = (req, res) => {
    Promise.all(req.body.map(sery => {
        Series.create({
            name: sery.series
        })
            .then(createdSery => {
                Promise.all(sery.groups.map(groupName => {
                        Group.create({
                            series_id: createdSery.id,
                            name: groupName
                        })

                    })
                )
            })
    }))
        .then(() => {
            res.status(201).send('Success')
        });
};

module.exports.addCustomGroup = async (req, res) => {
    const subject = await model.Subject.findOne({where: {name: req.body.subject}, raw: true});
    model.CustomGroup.create({
        name: req.body.name,
        subject_id: subject.id,
        leader: req.session.id
    }).then(async (customGroup) => {
        await model.UserCustomGroup.create({
            user_id: req.session.id,
            custom_group_id: customGroup.id
        })


        res.status(200).send('Success');
    })
};

module.exports.getCustomGroups = async (req, res) => {
    const customGroups = await model.CustomGroup.findAll({raw: true});
    let array = [];
    array = await Promise.all(customGroups.map(async (customGroup) => {
            const leader = await model.User.findOne({
                attributes: ['name', 'email'],
                where: {id: customGroup.leader}, raw: true
            });
            let obj = {name: customGroup.name, leader: leader, users: []};
            const customUserGroups = await model.UserCustomGroup.findAll({
                where: {custom_group_id: customGroup.id},
                raw: true
            });
            Promise.all(customUserGroups.map(async y => {

                obj.users.push(y)
            }))

            return obj;
        })
    );

    res.status(200).send(array);
}