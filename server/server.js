const express = require('express');
const bodyParser = require('body-parser');
const router = require('./routes/index');
const authRouter = require('./routes/auth');
const path = require('path');
const PORT = process.env.PORT || 8080;
const session = require('client-sessions');


const key = require('../config').development.key;



const app = express();


app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));

app.get('/*', function (req, res, next) {
    res.header('Cache-Control', 'no-cache, no-store');
    next();
});

app.use(session({
    cookieName: 'session',
    secret:key.session ,
    duration: 7200000,
    activeDuration: 300000,
    httpOnly: true,
    ephemeral: true
}));


app.use('/auth',authRouter);
app.use('/api',  router);
app.get('/', (req, res) => {
    res.sendFile(path.resolve(__dirname, '../.', 'client', 'index.html'));
});


app.use(express.static(path.resolve(__dirname, '..', 'client')));



app.listen(PORT, () => {
    console.log(`server is running on http://localhost:${PORT}.`);
});
