const express = require('express')
const router = express.Router();
const Middleware = require('../controllers/middlewares');
const defaultController = require('.././controllers/default');


 router.post('/login', defaultController.initialLogin);
 router.post('/register', defaultController.register);
router.get('/create', defaultController.recreateTables);

module.exports = router;