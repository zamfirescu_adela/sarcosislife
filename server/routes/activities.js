const express = require('express')
const router = express.Router();
const activitiesController =require('../controllers/activities');
const Middleware = require('../controllers/middlewares');


router.post('/add', activitiesController.addActivity);
router.post('/massAdd', activitiesController.addActivities);
router.get('/get', activitiesController.getActivities);


module.exports = router;
