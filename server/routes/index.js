let express = require('express');
let router = express.Router();
let defaultController = require('../controllers/default');
let middleware = require('../controllers/middlewares');


router.get('/logout', defaultController.Logout);

router.use('/groups', require('./groups'));
router.use('/activities', middleware.LogInCheck, require('./activities'));


router.get('*', function (req, res) {
    res.status(404).send('Nothing found');
});
module.exports = router;