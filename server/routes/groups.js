const express = require('express')
const router = express.Router();
const groupController =require('../controllers/groups');
const middleware = require('../controllers/middlewares');


router.post('/add', groupController.addSeriesAndGroups);
router.get('/get', groupController.getGroups);

router.post('/addCustom',middleware.LogInCheck,groupController.addCustomGroup);
router.get('/getCustom',groupController.getCustomGroups);

module.exports = router;
