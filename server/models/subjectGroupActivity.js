'use strict';

module.exports = function (connect, DataTypes) {
    return connect.define('subjectGroupActivity', {
        name:DataTypes.STRING,
        type: DataTypes.STRING,
        start:DataTypes.STRING,
        deadline:DataTypes.STRING,
        points: DataTypes.FLOAT,
        finished:DataTypes.BOOLEAN
    }, {
        underscored: true
    });

};