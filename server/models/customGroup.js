'use strict';

module.exports = function (connect, DataTypes) {
    return connect.define('customGroup', {
        name: DataTypes.STRING,
    }, {
        underscored: true
    });

};