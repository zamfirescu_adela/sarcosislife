'use strict';

module.exports = function (connect, DataTypes) {
    return connect.define('subjectGroup', {
        type: DataTypes.STRING
    }, {
        underscored: true
    });

};