'use strict';

module.exports = function (connect, DataTypes) {
    return connect.define('series', {
        name: DataTypes.STRING,
    }, {
        underscored: true
    });

};