'use strict';
const connect = require('../conf/db');

const ClassRoom = connect.import('./classRoom');
const CustomGroup = connect.import('./customGroup');
const Group = connect.import('./group');
const Professor = connect.import('./professor');
const Series = connect.import('./series');
const Subject = connect.import('./subject');
const SubjectGroup = connect.import('./subjectGroup');
const SubjectGroupActivity = connect.import('./subjectGroupActivity');
const SubjectSeries = connect.import('./subjectSeries');
const SubjectSeriesActivity = connect.import('./subjectSeriesActivity');
const User = connect.import('./user');
const UserCustomGroup = connect.import('./userCustomGroup');
const SubjectCustomGroupActivity = connect.import('./subjectCustomGroupActivity');


Series.hasMany(Group,{onDelete:'cascade'});
Group.hasMany(User,{onDelete:'cascade'});
Series.hasMany(User,{onDelete:'cascade'});
UserCustomGroup.belongsTo(User, {onDelete: 'cascade'});
UserCustomGroup.belongsTo(CustomGroup, {onDelete: 'cascade'});

CustomGroup.belongsTo(User,{onDelete:'cascade', foreignKey: 'leader'});
CustomGroup.belongsTo(Subject,{onDelete:'cascade'})

SubjectSeries.belongsTo(ClassRoom,{onDelete:'cascade'});
SubjectSeries.belongsTo(Professor,{onDelete:'cascade'});
SubjectSeries.belongsTo(Subject,{onDelete:'cascade'});
SubjectSeries.belongsTo(Series,{onDelete:'cascade'});

SubjectGroup.belongsTo(ClassRoom,{onDelete:'cascade'});
SubjectGroup.belongsTo(Professor,{onDelete:'cascade'});
SubjectGroup.belongsTo(Subject,{onDelete:'cascade'});
SubjectGroup.belongsTo(Group,{onDelete:'cascade'});

SubjectGroupActivity.belongsTo(Group,{onDelete:'cascade'});
SubjectSeriesActivity.belongsTo(Series,{onDelete:'cascade'});
SubjectCustomGroupActivity.belongsTo(CustomGroup,{onDelete:'cascade'});



module.exports = {
    User,
    ClassRoom,
    CustomGroup,
    Group,
    Professor,
    Series,
    Subject,
    SubjectGroup,
    SubjectGroupActivity,
    SubjectSeries,
    SubjectSeriesActivity,
    UserCustomGroup,
    SubjectCustomGroupActivity,
    connect
};
