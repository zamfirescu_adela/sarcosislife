'use strict';

module.exports = function (connect, DataTypes) {
    return connect.define('subject', {
        name: DataTypes.STRING,
        noCredit:DataTypes.INTEGER,
        year:DataTypes.INTEGER,
        semester:DataTypes.INTEGER,
        mandatory:DataTypes.BOOLEAN,
    }, {
        underscored: true
    });

};