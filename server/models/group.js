'use strict';

module.exports = function (connect, DataTypes) {
    return connect.define('group', {
        name: DataTypes.STRING,
    }, {
        underscored: true
    });

};