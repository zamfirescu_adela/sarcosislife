'use strict';

module.exports = function (connect, DataTypes) {
    return connect.define('user', {
        name: DataTypes.STRING,
        privileges:DataTypes.STRING,
        password:DataTypes.STRING,
        email:DataTypes.STRING,
        token:DataTypes.STRING

    }, {
        underscored: true
    });

};