'use strict';

module.exports = function (connect, DataTypes) {
    return connect.define('professor', {
        name: DataTypes.STRING,
        email:DataTypes.STRING,
    }, {
        underscored: true
    });

};