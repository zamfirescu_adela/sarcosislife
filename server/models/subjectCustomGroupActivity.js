'use strict';

module.exports = function (connect, DataTypes) {
    return connect.define('subjectCustomGroupActivity', {
        name:DataTypes.STRING,
        type: DataTypes.STRING,
        start:DataTypes.STRING,
        deadline:DataTypes.STRING,
        finished:DataTypes.BOOLEAN
    }, {
        underscored: true
    });

};