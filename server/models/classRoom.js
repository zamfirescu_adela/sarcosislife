'use strict';

module.exports = function (connect, DataTypes) {
    return connect.define('classRoom', {
        code: DataTypes.STRING,
        building:DataTypes.STRING,
        type:DataTypes.STRING,
    }, {
        underscored: true
    });

};