const Sequelize = require('sequelize');
const config = require('../../config');

let db;
 db = config.development.db;



const connect = new Sequelize(db.database, db.username, db.password, {
    dialect: db.dialect,
    host: db.host,
    define: {
        timestamps: false
    }
});

module.exports = connect;